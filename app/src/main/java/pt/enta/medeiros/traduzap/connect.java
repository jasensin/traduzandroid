package pt.enta.medeiros.traduzap;

import android.os.AsyncTask;
import android.widget.EditText;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by TPSI on 18-07-2016.
 */
public class connect extends AsyncTask<Integer, Integer, String> {
    private int frase = 0;
    private int lingua = 0;
    protected ObjectOutputStream output;
    protected ObjectInputStream input;
    protected Socket socket;
    protected Protocolo protocolo;
    protected String serverIP = "192.168.132.51";
    public String resposta;

    public connect(int frase, int lingua) {
        this.frase = frase;
        this.lingua = lingua;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected String doInBackground(Integer... ints) {
        try {
            socket = new Socket(serverIP, 9009);
            System.out.println("Ligação: " + socket.toString());
            resposta = sendMessage(frase, lingua);

        } catch (IOException ex) {
            Logger.getLogger(connect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resposta;
    }

    private String sendMessage(int frase, int lingua) {
        String resposta = null;
        try {
            output = new ObjectOutputStream(socket.getOutputStream());
            switch (lingua) {
                case 0:
                    protocolo = new Protocolo(Protocolo.EN, frase);
                    break;
               /* case 1:
                    protocolo = new Protocolo(Protocolo.FR, frase);
                    break;
                case 2:
                    protocolo = new Protocolo(Protocolo.AL, frase);
                    break;*/
            }
            output.writeObject(protocolo);
            input = new ObjectInputStream(socket.getInputStream());
            //  jTextArea2.setText(input.readObject().toString());
            resposta = input.readObject().toString();
            System.out.println(resposta);
            socket.close();
            return resposta;
        } catch (IOException ex) {
            Logger.getLogger(connect.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return resposta;
    }
}
