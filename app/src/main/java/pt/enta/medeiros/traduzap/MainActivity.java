package pt.enta.medeiros.traduzap;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.concurrent.ExecutionException;

import pt.enta.medeiros.traduzirap.R;

public class MainActivity extends AppCompatActivity {
    private String[] arraySpinner;
    private String[] arraySpinnerDE;
    private String[] arraySpinnerPara;
    Spinner sFrase;
    Spinner sDe;
    Spinner sPara;
    public EditText resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.arraySpinner = new String[]{
                "Bem Vindo a minha aplicação",
                "Programar é Divertido",
                "A Joana foi as compras",
                "Era uma vez um cao",
                "Portugal venceu o Europeu",
                "A Ponta Delgada é muito bonita",
                "Isto é uma frase",
                "A Maria foi as compras",
                "Se eu nao fizer o teste tenho negativa",
                "Não tinha mais ideias para fazer uma frase",
        };
        sFrase = (Spinner) findViewById(R.id.spinnerFrase);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        sFrase.setAdapter(adapter);
        this.arraySpinnerDE = new String[]{
                "PT",
        };
        sDe = (Spinner) findViewById(R.id.SpinnerDe);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinnerDE);
        sDe.setAdapter(adapter2);
        this.arraySpinnerPara = new String[]{
                "EN",
        };
        sPara = (Spinner) findViewById(R.id.spinnerPara);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinnerPara);
        sPara.setAdapter(adapter3);

        resultado = (EditText)findViewById(R.id.eTTraduzido);
    }


    public void btnClick(View v) throws ExecutionException, InterruptedException {
        int frase = sFrase.getSelectedItemPosition();
        int lingua = sPara.getSelectedItemPosition();
        AsyncTask<Integer, Integer, String> connect = new connect(frase,lingua).execute();
        EditText original = (EditText)findViewById(R.id.eTOriginal);
        original.setText(sFrase.getSelectedItem().toString());
        System.out.println(connect.get());
        resultado.setText(connect.get().toString());
    }


}
